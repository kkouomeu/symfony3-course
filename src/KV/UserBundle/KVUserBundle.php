<?php

namespace KV\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class KVUserBundle extends Bundle
{

    public function getParent()
    {
       return 'FOSUserBundle';
    }
}
