<?php
/**
 * Created by PhpStorm.
 * User: arsenekevin
 * Date: 22/09/2019
 * Time: 12:06
 */

namespace KV\UserBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KV\UserBundle\Entity\User;


class LoadUser implements  FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $listNames = array(
            'Martial',
            'Kevin',
            'Bernard',
            'Alphonse'
        );

        foreach ($listNames as $name){
            $user = new User();
            $user->setUsername($name);
            $user->setPassword($name);
            $user->setSalt('');
            $user->setRoles(array('ROLE_USER'));

            $manager->persist($user);
        }

        $manager->flush();
    }

}