<?php
/**
 * Created by PhpStorm.
 * User: arsenekevin
 * Date: 18/03/2019
 * Time: 13:41
 */

namespace KV\MyBundle\Services\Email;


use KV\MyBundle\Entity\Application;

class ApplicationMailer
{

    /**
     * @Var \Swift_mailer
     */
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }


    public function sendNewNotification(Application $application)
    {
        $message = new \Swift_Message(
            'Nouvelle candidature',
            'Vous avez reçu une nouvelle candidature du cours OCR | Symfony 💻'
        );

        $message->addTo($application->getAdvert()->getEmail())
                ->addFrom('simokevin95@outlook.fr');

        $this->mailer->send($message);
    }
}