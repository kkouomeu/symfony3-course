<?php
/**
 * Created by PhpStorm.
 * User: arsenekevin
 * Date: 01/02/2019
 * Time: 18:49
 */

namespace KV\MyBundle\Services\Antispam;


class KVAntispam
{

    private $mailer;
    private $locale;
    private $minLength;


    public function __construct(\Swift_Mailer $mailer, $locale, $minLength)
    {
        $this->mailer = $mailer;
        $this->locale = $locale;
        $this->minLength = $minLength;
    }


    public function isSpam($text)
    {
        $result = strlen($text) < $this->minLength;
        return $result;
    }
}

