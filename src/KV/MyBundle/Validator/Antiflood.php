<?php
/**
 * Created by PhpStorm.
 * User: arsenekevin
 * Date: 08/08/2019
 * Time: 12:17
 */

namespace KV\MyBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Antiflood extends Constraint
{
    public $message = "Vous avez déjà posté un message il y'a moins de 15 secondes, merci d'attendre un peu";

    public function validatedBy()
    {
        return 'kv_my_antiflood';
    }
}