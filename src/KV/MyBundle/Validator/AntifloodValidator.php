<?php
/**
 * Created by PhpStorm.
 * User: arsenekevin
 * Date: 08/08/2019
 * Time: 12:33
 */

namespace KV\MyBundle\Validator;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class AntifloodValidator extends ConstraintValidator
{


    private $requestStack;
    private $em;

    public function __construct(RequestStack $requestStack, EntityManagerInterface $em)
    {
        $this->requestStack = $requestStack;
        $this->em           = $em;
    }


    public function validate($value, Constraint $constraint)
    {


       /* // ------ VALIDATION PAR UN SERVICE DÉCLARÉ ------- //

       $request = $this->requestStack->getCurrentRequest();

        $ip = $request->getClientIp();

        $isFlood = $this->em
            ->getRepository('KVMyBundle:Application')
            ->isFlood($ip, 15);

        if($isFlood)
        {
            $this->context->addViolation($constraint->message);
        }*/

        //on considere comme invalide tout message de moins de 3 caractères
        if(strlen($value) < 3)
        {
            $this->context->addViolation($constraint->message);
        }
    }
}