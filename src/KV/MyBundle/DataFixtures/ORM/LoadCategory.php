<?php
/**
 * Created by PhpStorm.
 * User: arsenekevin
 * Date: 09/03/2019
 * Time: 14:15
 */

namespace KV\MyBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KV\MyBundle\Entity\Category;

class LoadCategory implements FixtureInterface
{
    //L'objet $manager est l'entity manager
    public function load(ObjectManager $manager)
    {
        $names = array(
            'Developpement web',
            'Developpement mobile',
            'Graphisme',
            'Intégration',
            'Réseau',
            'Autre'
        );

        foreach ($names as $name) {
            $category = new Category();
            $category->setName($name);

            $manager->persist($category);
        }

        $manager->flush();

    }
}