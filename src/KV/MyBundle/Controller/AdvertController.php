<?php

// src/OC/PlatformBundle/Controller/AdvertController.php

namespace KV\MyBundle\Controller;

use KV\MyBundle\Entity\Advert;
use KV\MyBundle\Entity\AdvertSkill;
use KV\MyBundle\Entity\Application;
use KV\MyBundle\Entity\Image;
use KV\MyBundle\Form\AdvertEditType;
use KV\MyBundle\Form\AdvertType;
use KV\MyBundle\Repository\AdvertRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


class AdvertController extends Controller
{

    /**
     * @Route("/{page}", name="kv_platform_home",defaults={"page"=1},requirements={"page"="\d*"})
     */
    public function indexAction($page)
    {
        if ($page < 1) {
            throw new NotFoundHttpException('Page "'.$page.'" inexistante.');
        }

        $em = $this->getDoctrine()->getManager();
        $nbPerPage = 5;

        $listAdverts = $em->getRepository('KVMyBundle:Advert')->getAdverts($page,$nbPerPage);

        // On calcule le nombre total de pages grâce au count($listAdverts) qui retourne le nombre total d'annonces
        $nbPages = ceil(count($listAdverts) / $nbPerPage);

        // Si la page n'existe pas, on retourne une 404
        if ($page > $nbPages) {
            throw $this->createNotFoundException("La page ".$page." n'existe pas.");
        }

        return $this->render('@KVMy/Advert/index.html.twig', array(
            'listAdverts' => $listAdverts,
            'nbPages'     => $nbPages,
            'page'        => $page
            ));
    }



    /**
     * @Route("/advert/{id}", name="kv_platform_view",requirements={"id"="\d+"})
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        // On récupère l'entité correspondante à l'id $id
        $advert = $em->getRepository('KVMyBundle:Advert')->find($id);


        // $advert est donc une instance de OC\PlatformBundle\Entity\Advert
        // ou null si l'id $id  n'existe pas, d'où ce if :
        if (null === $advert) {
            throw new NotFoundHttpException("L'annonce d'id ".$id." n'existe pas.");
        }

        $applications = $em->getRepository('KVMyBundle:Application')->findBy(
            array(
                'advert' => $advert
            )
        );

        $listAdvertSkills = $em->getRepository('KVMyBundle:AdvertSkill')->findBy(
            array(
                'advert' => $advert
            )
        );

        // Le render ne change pas, on passait avant un tableau, maintenant un objet
        return $this->render('@KVMy/Advert/view.html.twig', array(
            'advert' => $advert,
            'applications' => $applications,
            'listAdvertSkills' => $listAdvertSkills
        ));
    }


    /**
     * @Route("/add", name="kv_platform_add")
     */
    public function addAction(Request $request)
    {
        if(!$this->get('security.authorization_checker')->isGranted('ROLE_AUTEUR')){
            throw  new AccessDeniedException('Accès limité aux auteurs');
        }


        // On crée un objet Advert
        $advert = new Advert();

        $advert->setEmail('simokevin95@outlook.fr');

        // On crée le formulaire grâce au service form factory
        $form = $this->createForm(AdvertType::class, $advert);

        if($request->isMethod('POST') && $form->handleRequest($request)->isValid() )
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($advert);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            return $this->redirectToRoute('kv_platform_view',array('id' => $advert->getId()));
        }

        return $this->render('@KVMy/Advert/add.html.twig', array(
            'form' => $form->createView(),
        ));

    }



    /**
     * @Route("/edit/{id}", name="kv_platform_edit",requirements={"id"="\d+"})
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $advertToUpdate = $em->getRepository('KVMyBundle:Advert')->find($id);

        if($advertToUpdate === null) {
            throw new NotFoundHttpException("L'annonce d'id : ".$id." n'existe pas");
        }

        $form = $this->createForm(AdvertEditType::class, $advertToUpdate);

        if($request->isMethod('POST') && $form->handleRequest($request)->isValid() )
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($advertToUpdate);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien Modifiée.');

            return $this->redirectToRoute('kv_platform_view',array('id' => $advertToUpdate->getId()));
        }

        return $this->render('@KVMy/Advert/edit.html.twig', array(
            'form' => $form->createView(),
            'advert' => $advertToUpdate
        ));
    }


    /**
     * @Route("/edit_cat/{id}", name="kv_platform_edit_cat",requirements={"id"="\d+"})
     */
    public function editCatAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $advert = $em->getRepository('KVMyBundle:Advert')->find($id);

        if($advert === null) {
            throw new NotFoundHttpException("L'annonce d'id : ".$id." n'existe pas");
        }

        $listCategories = $em->getRepository('KVMyBundle:Category')->findAll();
        foreach ($listCategories as $category){
            $advert->removeCategory($category);
        }

        $em->flush();
        $request->getSession()->getFlashBag()->add('deleted','Catégories bien supprimées !');




        return $this->redirectToRoute('kv_platform_view',array('id' => $advert->getId()));
    }



    /**
     * @Route("/delete/{id}", name="kv_platform_delete",requirements={"id"="\d+"})
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $advert = $em->getRepository('KVMyBundle:Advert')->find($id);

        if($advert === null) {
            throw new NotFoundHttpException("L'annonce d'id : ".$id." n'existe pas");
        }

        $form = $this->get('form.factory')->create();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid())
        {
            $em->remove($advert);
            $em->flush();

            $request->getSession()->getFlashBag()->add('info','L\'annonce a bien été supprimée' );

            return $this->redirectToRoute('kv_platform_home');
        }

        return $this->render('@KVMy/Advert/delete.html.twig', array(
            'advert' => $advert,
            'form' => $form->createView()
        ));
    }



    public function menuAction()
    {
        $em = $this->getDoctrine()->getManager();

        $listAdverts = $em->getRepository('KVMyBundle:Advert')->findBy(
            array(),
            array('date' => 'desc')
        );

        return $this->render('@KVMy/Advert/menu.html.twig', array(
            'listAdverts' => $listAdverts
        ));
    }

    /**
     * @Route("/purge/{days}", name="kv_platform_purge",requirements={"days"="\d+"})
     */
    public function purgeAction( $days )
    {

    }



}


