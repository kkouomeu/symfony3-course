<?php

namespace KV\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CoreController extends Controller
{
    public function indexAction()
    {
        return $this->render('@KVCore/Default/index.html.twig');
    }

    public function contactAction(Request $request)
    {
        $session = $request->getSession();
        $session->getFlashBag()->add('noContact', 'La page de contact n\'est pas encore disponible, merci de revenir plutard');

        return $this->render('@KVCore/contact.html.twig');
    }
}
