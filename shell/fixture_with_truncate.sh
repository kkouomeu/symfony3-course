php bin/console doctrine:database:drop --force 
php bin/console doctrine:database:create -v
php bin/console doctrine:schema:update --force 
php bin/console doctrine:fixtures:load -v
